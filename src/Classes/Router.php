<?php
namespace BIT703\Classes;
 
/*
 * Class to process requests.
 *
 * @package BIT703
 * @license GPL
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author Open Polytechnic <info@openpolytechnic.ac.nz>
 * @copyright Open Polytechnic, 2018
 */
class Router
{
 
    /*
     * The controller 
     * being called.
     *
     * @var object
     */
    private $controller;
 
    /*
     * The method 
     * being requested.
     *
     * @var object
     */
    private $method;
 
    /*
     * The GET Request 
     *
     * @var object
     */
    private $request;
 
    /*
     * Assigns the matching $_GET parameters 
     * passed in via the .htaccess file like so:
     * 
     * /user/login is passed on the $_GET string as
     *      $_GET['controller'] = 'user',
     *      $_GET['method'] = 'user'
     * 
     * @return void
     */
    public function __construct(array $request)
    {
 
    }
}