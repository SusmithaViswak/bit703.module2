<?php
require('C:\wamp64\www\bit703\module2\src\App\bootstrap.php');

use \BIT703\Classes\Router as Router;

class RouterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    private $router;
    
    protected function _before()
    {
        $request = [
            'get' => $_GET,
            'post' => $_POST,
            'file' => $_FILES
        ];
        $this->router = new Router($request);
    }

    protected function _after()
    {
    }

    // tests
    public function testSRouter()
    {
        $this->assertInstanceOf('\BIT703\Classes\Router', $this->router);

    }
}